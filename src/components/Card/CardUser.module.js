const { makeStyles } = require("@material-ui/core");

export const useStyle = makeStyles((theme) => ({
  fullCard: {
    width: "100%",
    height: "13rem",
    margin: theme.spacing(3, 0),
  },
  textProfessional: {
    background: "green",
    color: "white",
  },
  textPersonal: {
    background: "blue",
    color: "white",
  },
  items: {
    display: "flex",
    margin: theme.spacing(2, 0),
  },
  btnEdit: {
    background: "gray",
    color: "white",
    "&:hover": {
      cursor: "pointer",
    },
  },
  btnDelete: {
    background: "red",
    color: "white",
    marginLeft: theme.spacing(1),
    "&:hover": {
      cursor: "pointer",
    },
  },
}));
