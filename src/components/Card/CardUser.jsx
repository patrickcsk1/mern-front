import {
  Button,
  Card,
  CardContent,
  Chip,
  Grid,
  Typography,
} from "@material-ui/core";
import DraftsIcon from "@material-ui/icons/Drafts";
import PhoneIcon from "@material-ui/icons/Phone";
import React, { useContext, useState } from "react";
import swal from "sweetalert2";
import { MainContext } from "../../context";
import { userDelete as userDeleteApi } from "../../utils/apis";
import DialogEdit from "../Dialog/DialogEdit";
import { useStyle } from "./CardUser.module";

const CardUser = (props) => {
  const { user } = props;
  const [open, setOpen] = useState(false);
  const context = useContext(MainContext);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deleteUser = async (item) => {
    await swal
      .fire({
        title: "Borrar Usuario",
        text: "¿Seguro que desea borrar al usuario?",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        showCancelButton: true,
        icon: "question",
      })
      .then(async (result) => {
        if (result.value) {
          console.log(user);
          const response = await userDeleteApi(user._id);
          if (response.status === 200) {
            await swal.fire({
              title: "Borrar Usuario",
              text: "Usuario borrado con exito",
              confirmButtonText: "Aceptar",
              cancelButtonText: "Cancelar",
              showCancelButton: true,
              icon: "success",
            });
            var aux = context.flag;
            aux += 1;
            context.setFlag(aux);
          }
        }
      });
  };

  const classes = useStyle();
  return (
    <Card className={classes.fullCard}>
      <CardContent>
        <Grid container direction="row" justify="space-between">
          <Typography variant="h6">{user.fullname}</Typography>
          <Chip
            label={user.type}
            className={`${
              user.type === "Professional"
                ? classes.textProfessional
                : classes.textPersonal
            }`}
          />
        </Grid>
        <div className={classes.items}>
          <DraftsIcon />
          <Typography variant="body2">{user.email}</Typography>
        </div>
        <div className={classes.items}>
          <PhoneIcon />
          <Typography variant="body2">{user.phone}</Typography>
        </div>
        <div>
          <Button
            variant="contained"
            className={classes.btnEdit}
            onClick={() => handleOpen()}
          >
            Edit
          </Button>
          <Button
            variant="contained"
            className={classes.btnDelete}
            onClick={() => deleteUser()}
          >
            Delete
          </Button>
        </div>
      </CardContent>
      {open && <DialogEdit open={open} handleClose={handleClose} />}
    </Card>
  );
};

export default CardUser;
