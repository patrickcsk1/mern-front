import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";

const DialogEdit = (props) => {
  const { open, handleClose } = props;
  //   const handleClose = () => onClose();

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      maxWidth="md"
      open={open}
    >
      <DialogTitle id="simple-dialog-title">Editar usuario</DialogTitle>
      <DialogContent dividers>
        <Typography variant="subtitle2" gutterBottom>
          Llene los campos obligatorios
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              //   error={userError.name}
              id="name"
              name="name"
              label="Nombres"
              //   value={selectedUser.name}
              //   onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="given-name"
            />
          </Grid>
          {/* <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              error={userError.apPaterno}
              id="apPaterno"
              name="apPaterno"
              label="Apellido Paterno"
              value={selectedUser.apPaterno}
              onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="family-name"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              error={userError.apMaterno}
              id="apMaterno"
              name="apMaterno"
              label="Apellido Materno"
              value={selectedUser.apMaterno}
              onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="family-name"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              error={userError.email}
              id="email"
              name="email"
              label="Correo"
              value={selectedUser.email}
              onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="email"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              error={userError.telephone}
              id="telephone"
              name="telephone"
              label="Teléfono"
              value={selectedUser.telephone}
              onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="tel-local"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              autoFocus
              error={userError.enterprise}
              id="enterprise"
              name="enterprise"
              label="Empresa"
              value={selectedUser.enterprise}
              onChange={handleChange}
              fullWidth
              size="small"
              autoComplete="organization"
            />
          </Grid> */}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          //   className={classes.buttonPrimary}
          //   onClick={() => handleUpdateUser()}
        >
          Agregar
        </Button>
        <Button
          variant="outlined"
          //   className={classes.buttonRed}
          onClick={handleClose}
        >
          Cerrar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogEdit;
