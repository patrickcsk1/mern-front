import {
  Button,
  Container,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";
import { useStyle } from "./AddView.module";

const AddView = (props) => {
  const classes = useStyle();
  const { user, handleChange, handleRadio, addUser } = props;

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography variant="h5" color="primary">
          Add Contact
        </Typography>
        <form className={classes.form} onSubmit={addUser}>
          <TextField
            autoFocus
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={user.fullname}
            onChange={(e) => handleChange(e)}
            name="fullname"
            label="Name"
            type="text"
            id="fullname"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={user.email}
            onChange={(e) => handleChange(e)}
            id="email"
            label="E-mail"
            name="email"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={user.phone}
            onChange={(e) => handleChange(e)}
            id="phone"
            label="Phone"
            name="phone"
          />
          <Typography variant="body2">Contact Type</Typography>
          <RadioGroup
            row
            aria-label="position"
            name="position"
            defaultValue="top"
            value={user.type}
            onChange={(e) => handleRadio(e)}
          >
            <FormControlLabel
              value="Personal"
              control={<Radio color="primary" />}
              label="Personal"
              labelPlacement="end"
            />
            <FormControlLabel
              value="Professional"
              control={<Radio color="primary" />}
              label="Professional"
              labelPlacement="end"
            />
          </RadioGroup>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Add Contact
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default AddView;
