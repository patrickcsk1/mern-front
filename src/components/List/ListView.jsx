import { Container, Grid, TextField } from "@material-ui/core";
import React from "react";
import CardUser from "../Card/CardUser";
import { useStyle } from "./ListView.module";

const ListView = (props) => {
  const classes = useStyle();
  const { textSearch, handleChange, listUsers } = props;

  return (
    <Container component="main" maxWidth="sm">
      <TextField
        autoFocus
        variant="outlined"
        margin="normal"
        required
        fullWidth
        value={textSearch}
        onChange={(e) => handleChange(e)}
        placeholder="Search . . ."
        type="text"
        id="fullname"
      />
      {listUsers &&
        listUsers.map((item) => (
          <Grid item xs={12} sm={12} key={item._id}>
            <CardUser user={item} />
          </Grid>
        ))}
    </Container>
  );
};

export default ListView;
