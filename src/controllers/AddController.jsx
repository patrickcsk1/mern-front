import React, { useContext, useState } from "react";
import swal from "sweetalert2";
import AddView from "../components/Add/AddView";
import { MainContext } from "../context";
import { userAdd as userAddApi } from "../utils/apis";

const initUser = {
  fullname: "",
  email: "",
  phone: "",
  type: "Personal",
};

const AddController = () => {
  const [user, setUser] = useState(initUser);
  const context = useContext(MainContext);

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const handleRadio = (e) => {
    setUser({ ...user, type: e.target.value });
  };

  const addUser = async (e) => {
    e.preventDefault();
    console.log("addUser", user);
    if (
      user.fullname.trim() === "" &&
      user.email.trim() === "" &&
      user.phone.trim() === ""
    ) {
      await swal.fire({
        title: "Agregar Usuario",
        text: "Campos vacios",
        icon: "error",
      });
    } else if (!Number(user.phone)) {
      await swal.fire({
        title: "Agregar Usuario",
        text: "El telefono debe ser número",
        icon: "error",
      });
    } else {
      await swal
        .fire({
          title: "Agregar Usuario",
          text: "¿Seguro que desea agregar al usuario?",
          confirmButtonText: "Aceptar",
          cancelButtonText: "Cancelar",
          showCancelButton: true,
          icon: "question",
        })
        .then(async (result) => {
          if (result.value) {
            var obj = {
              fullname: user.fullname,
              email: user.email,
              phone: user.phone,
              type: user.type,
            };
            console.log(obj);
            var resp = await userAddApi(obj);
            console.log("addUser -> resp", resp);
            if (resp.status === 200) {
              await swal.fire({
                title: "Agregar Usuario",
                text: "Usuario agregado correctamente",
                icon: "success",
              });
              var aux = context.flag;
              aux += 1;
              context.setFlag(aux);
              setUser(initUser);
            }
          }
        });
    }
  };

  return (
    <AddView
      user={user}
      handleChange={handleChange}
      handleRadio={handleRadio}
      addUser={addUser}
    />
  );
};

export default AddController;
