import React, { useContext, useEffect, useState } from "react";
import ListView from "../components/List/ListView";
import { MainContext } from "../context";
import { userList as userListApi } from "../utils/apis";

const ListContainer = () => {
  const [textSearch, setTextSearch] = useState("");
  const [listUsers, setListUsers] = useState([]);
  const context = useContext(MainContext);

  const handleChange = (e) => {
    setTextSearch(e.target.value);
  };

  const getList = async () => {
    const response = await userListApi();
    if (response) setListUsers(response.data);
    else setListUsers([]);
  };

  useEffect(() => {
    getList();
  }, [context.flag]);

  return (
    <ListView
      textSearch={textSearch}
      handleChange={handleChange}
      listUsers={listUsers}
    />
  );
};

export default ListContainer;
