import React from "react";
import AddController from "./controllers/AddController";
import ListContainer from "./controllers/ListContainer";

function App() {
  return (
    <div style={{ flexGrow: "1" }}>
      <AddController />
      <ListContainer />
    </div>
  );
}

export default App;
