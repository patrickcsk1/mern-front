import React, { createContext, useState } from "react";
export const MainContext = createContext();

export const MainProvider = (props) => {
  const [flag, setFlag] = useState(0);
  var obj = { flag, setFlag };
  return (
    <MainContext.Provider value={obj}>{props.children}</MainContext.Provider>
  );
};
