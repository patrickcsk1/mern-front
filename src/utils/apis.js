import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:4000/api/users",
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST,GET",
    Accept: "*/*",
  },
});

export const userList = async () => {
  const response = await api({
    method: "get",
    url: "/list",
  });
  if (response.status == 200) return response.data;
  else return null;
};

export const userGetOne = async (idUser) => {
  const response = await api({
    method: "post",
    url: "/getOne/" + idUser,
  });
  if (response.status == 200) return response.data;
  else return null;
};

export const userAdd = async (body) => {
  const response = await api({
    method: "post",
    url: "/add",
    data: body,
  });
  if (response.status == 200) return response.data;
  else return null;
};

export const userEdit = async (idUser, body) => {
  const response = await api({
    method: "post",
    url: "/edit/" + idUser,
    data: body,
  });
  if (response.status == 200) return response.data;
  else return null;
};

export const userDelete = async (idUser) => {
  const response = await api({
    method: "post",
    url: "/delete/" + idUser,
  });
  if (response.status == 200) return response.data;
  else return null;
};
